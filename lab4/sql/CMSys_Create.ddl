CREATE TABLE Course (courseId int(10) NOT NULL AUTO_INCREMENT, name varchar(255), description varchar(255), isNewCourse tinyint(1), Course_GroupcourseGroupId int(10) NOT NULL, PRIMARY KEY (courseId));
CREATE TABLE Course_Group (courseGroupId int(10) NOT NULL AUTO_INCREMENT, info varchar(255), PRIMARY KEY (courseGroupId));
CREATE TABLE Role (roleId int(10) NOT NULL AUTO_INCREMENT, roleName varchar(255), PRIMARY KEY (roleId));
CREATE TABLE Role_User (RoleroleId int(10) NOT NULL, UseruserId int(10) NOT NULL, PRIMARY KEY (RoleroleId, UseruserId));
CREATE TABLE Trainer (trainerId int(10) NOT NULL AUTO_INCREMENT, trainerInfo varchar(255), userId int(10), UseruserId int(10) NOT NULL, Trainer_GrouptrainerGroupId int(10) NOT NULL, PRIMARY KEY (trainerId));
CREATE TABLE Trainer_Course (TrainertrainerId int(10) NOT NULL, CoursecourseId int(10) NOT NULL, PRIMARY KEY (TrainertrainerId, CoursecourseId));
CREATE TABLE Trainer_Group (trainerGroupId int(10) NOT NULL AUTO_INCREMENT, info varchar(255), PRIMARY KEY (trainerGroupId));
CREATE TABLE `User` (userId int(10) NOT NULL AUTO_INCREMENT, email varchar(255), password varchar(255), firstName varchar(255), lastName varchar(255), startDate date, endDate date, department varchar(255), office varchar(255), PRIMARY KEY (userId));
ALTER TABLE Trainer ADD CONSTRAINT FKTrainer701476 FOREIGN KEY (Trainer_GrouptrainerGroupId) REFERENCES Trainer_Group (trainerGroupId);
ALTER TABLE Course ADD CONSTRAINT FKCourse754268 FOREIGN KEY (Course_GroupcourseGroupId) REFERENCES Course_Group (courseGroupId);
ALTER TABLE Trainer_Course ADD CONSTRAINT FKTrainer_Co215575 FOREIGN KEY (CoursecourseId) REFERENCES Course (courseId);
ALTER TABLE Trainer_Course ADD CONSTRAINT FKTrainer_Co791533 FOREIGN KEY (TrainertrainerId) REFERENCES Trainer (trainerId);
ALTER TABLE Trainer ADD CONSTRAINT FKTrainer768593 FOREIGN KEY (UseruserId) REFERENCES `User` (userId);
ALTER TABLE Role_User ADD CONSTRAINT FKRole_User413438 FOREIGN KEY (UseruserId) REFERENCES `User` (userId);
ALTER TABLE Role_User ADD CONSTRAINT FKRole_User97419 FOREIGN KEY (RoleroleId) REFERENCES Role (roleId);

