INSERT INTO Course(courseId, name, description, isNewCourse, Course_GroupcourseGroupId) VALUES (?, ?, ?, ?, ?);
INSERT INTO Course_Group(courseGroupId, info) VALUES (?, ?);
INSERT INTO Role(roleId, roleName) VALUES (?, ?);
INSERT INTO Role_User(RoleroleId, UseruserId) VALUES (?, ?);
INSERT INTO Trainer(trainerId, trainerInfo, userId, UseruserId, Trainer_GrouptrainerGroupId) VALUES (?, ?, ?, ?, ?);
INSERT INTO Trainer_Course(TrainertrainerId, CoursecourseId) VALUES (?, ?);
INSERT INTO Trainer_Group(trainerGroupId, info) VALUES (?, ?);
INSERT INTO `User`(userId, email, password, firstName, lastName, startDate, endDate, department, office) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);

