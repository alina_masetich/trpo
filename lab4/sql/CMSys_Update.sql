UPDATE Course SET name = ?, description = ?, isNewCourse = ?, Course_GroupcourseGroupId = ? WHERE courseId = ?;
UPDATE Course_Group SET info = ? WHERE courseGroupId = ?;
UPDATE Role SET roleName = ? WHERE roleId = ?;
UPDATE Role_User SET  WHERE RoleroleId = ? AND UseruserId = ?;
UPDATE Trainer SET trainerInfo = ?, userId = ?, UseruserId = ?, Trainer_GrouptrainerGroupId = ? WHERE trainerId = ?;
UPDATE Trainer_Course SET  WHERE TrainertrainerId = ? AND CoursecourseId = ?;
UPDATE Trainer_Group SET info = ? WHERE trainerGroupId = ?;
UPDATE `User` SET email = ?, password = ?, firstName = ?, lastName = ?, startDate = ?, endDate = ?, department = ?, office = ? WHERE userId = ?;

